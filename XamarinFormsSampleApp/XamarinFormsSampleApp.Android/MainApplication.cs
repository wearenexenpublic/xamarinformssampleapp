﻿using System;
using System.Collections.Generic;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.Runtime;
using Android.Support.V4.Content;
using BE.Wearenexen;
using BE.Wearenexen.Network;
using BE.Wearenexen.Network.Response.Beacon;
using Java.Lang;

namespace XamarinFormsSampleApp.Droid
{
    [Application]
    public class MainApplication : Application, INexenServiceInterface
    {
        private readonly NexenProximityManager.GeofenceBroadcastReceiver _geofenceBroadcastReceiver = new NexenProximityManager.GeofenceBroadcastReceiver();
        private readonly NexenProximityManager.GeofenceListBroadcastReceiver _geofenceListBroadcastReceiver = new NexenProximityManager.GeofenceListBroadcastReceiver();

        protected MainApplication(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public MainApplication()
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();
            RegisterReceiver(_geofenceBroadcastReceiver, new IntentFilter());
            RegisterReceiver(_geofenceListBroadcastReceiver, new IntentFilter());

            var manager = NexenProximityManager.Get();

            manager.ConfigureFor(this, this, new NexenProximitySetup("<username>", "<password>"));
            if (ContextCompat.CheckSelfPermission(Context, Manifest.Permission.AccessCoarseLocation) ==
                (int) Permission.Granted)
            {
                manager.Start();
            }
        }

        public override void OnTerminate()
        {
            base.OnTerminate();
            UnregisterReceiver(_geofenceListBroadcastReceiver);
            UnregisterReceiver(_geofenceBroadcastReceiver);
        }

        public void OnLocationsLoaded(IList<Location> p0, APIError p1)
        {

        }

        public void OnProximityZoneNotification(BaseNexenZone p0)
        {
            NexenNotificationBuilder.ShowNotificationForProximityZone(this,
                p0,
                Class.FromType(typeof(NotificationActivity)),
                Resource.Drawable.Icon,
                "XamarinBeaconApp", Color.Orange.ToArgb());
        }

        public void OnProximityZonesLoadedFromCache(ICollection<NexenBeaconZone> p0)
        {

        }

        public void OnRequirePermissions(IRunnable p0)
        {

        }
    }
}