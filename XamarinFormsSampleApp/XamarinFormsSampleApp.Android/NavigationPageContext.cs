﻿using System;

namespace XamarinFormsSampleApp.Droid
{
    public class NavigationPageContext
    {
        private static NavigationPageContext _instance;

        public static NavigationPageContext Instance => _instance ?? (_instance = new NavigationPageContext());

        public INavigationPageController Controller { get; private set; }

        private NavigationPageContext()
        {
        }

        public void RegisterController(INavigationPageController controller)
        {
            if (Controller == null) Controller = controller;
            else throw new Exception("Controller already registered in context");
        }
    }
}