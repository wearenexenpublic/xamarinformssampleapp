﻿using System.Collections.Generic;

namespace XamarinFormsSampleApp.Droid
{
    public class MessageStackContext
    {
        public enum MessageType
        {
            LocalNotification
        }

        public class Message
        {
            public Message(MessageType type, Dictionary<string, object> data)
            {
                Type = type;
                Data = data;
            }

            public MessageType Type { get; }
            public Dictionary<string, object> Data { get; }
        }

        private static MessageStackContext _instance;
        private readonly Stack<Message> _stack;

        public static MessageStackContext Instance => _instance ?? (_instance = new MessageStackContext());

        private MessageStackContext()
        {
            _stack = new Stack<Message>();
        }

        public void Push(Message message)
        {
            _stack.Push(message);
        }

        public int Count => _stack.Count;

        public Message Pop()
        {
            return _stack.Pop();
        }

        public Message Peek()
        {
            return _stack.Peek();
        }
    }
}