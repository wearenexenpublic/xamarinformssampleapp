﻿using System;
using Android;
using Android.Content.PM;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Xamarin.Forms;

[assembly: Dependency(typeof(XamarinFormsSampleApp.Droid.LocationPermissionHandler))]
namespace XamarinFormsSampleApp.Droid
{
    public class LocationPermissionHandler : ILocationPermissionHandler, ActivityCompat.IOnRequestPermissionsResultCallback
    {
        private const int RequestLocationId = 0;
        readonly string[] _permissionsLocation =
        {
            Manifest.Permission.AccessCoarseLocation
        };

        public void StartManagerOrShowRequestPermissions(INavigationPageController navigationPageController)
        {

            if (ContextCompat.CheckSelfPermission(MainActivity.Context, Manifest.Permission.AccessCoarseLocation) != (int)Permission.Granted)
            {
                BE.Wearenexen.NexenProximityManager.Get().Stop();

                //Finally request permissions with the list of permissions and Id
                ActivityCompat.RequestPermissions(MainActivity.Context, _permissionsLocation, RequestLocationId);
            }
            else
            {
                BE.Wearenexen.NexenProximityManager.Get().Start();
            }
        }

        public void StopManager()
        {
            BE.Wearenexen.NexenProximityManager.Get().Stop();
        }

        public void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            if (grantResults[0] == (int)Permission.Granted)
            {
                BE.Wearenexen.NexenProximityManager.Get().Start();
            }
        }

        public void Dispose()
        {
            
        }

        public IntPtr Handle { get; }
    }
}