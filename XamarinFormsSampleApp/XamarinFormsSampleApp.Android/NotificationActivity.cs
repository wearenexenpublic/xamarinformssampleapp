﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using BE.Wearenexen;

namespace XamarinFormsSampleApp.Droid
{
    [Activity(Label = "NotificationActivity", Theme = "@style/MainTheme")]
    public class NotificationActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Notification);
            
            var beaconId = Intent.Extras?.GetLong(Constants.BeaconIdIntentDataKey, long.MinValue) ?? long.MinValue;
            var contentId = Intent.Extras?.GetLong(Constants.ContentIdIntentDataKey, long.MinValue) ??  long.MinValue;

            if (beaconId == long.MinValue || contentId == long.MinValue)
            {
                Finish();
                return; //invalid notification => close NotificationActivity
            }

            //push the notification message on the stack (will be handled by MainActivity)
            MessageStackContext.Instance.Push(new MessageStackContext.Message(MessageStackContext.MessageType.LocalNotification, 
                new Dictionary<string, object>
            {
                    { Constants.BeaconIdIntentDataKey, beaconId },
                    { Constants.ContentIdIntentDataKey, contentId }
            }));

            
            //MainActivity is not started yet => start MainActivity 
            if (NavigationPageContext.Instance.Controller == null) 
            {
                var intent = new Intent(this, typeof(MainActivity));
                intent.SetFlags(ActivityFlags.NewTask);
                StartActivity(intent);
            }

            Finish(); //close NotificationActivity and go back to the MainActivity

        }
    }
}