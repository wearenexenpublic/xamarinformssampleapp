﻿using System;
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.OS;
using BE.Wearenexen;
using BE.Wearenexen.Network;
using BE.Wearenexen.Network.Callback;
using BE.Wearenexen.Network.Response.Beacon;
using BE.Wearenexen.Network.Response.Beacon.Content;
using Java.Lang;
using Object = Java.Lang.Object;

namespace XamarinFormsSampleApp.Droid
{
    [Activity(Label = "XamarinBeaconApp", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity, IResponseListener
    {
        public static Activity Context { get; private set; }

        public MainActivity()
        {
            Context = this;
        }

        public MainActivity(IntPtr handle, JniHandleOwnership transfer)
        {
            Context = this;
        }

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            var app = new XamarinFormsSampleApp.App();
            NavigationPageContext.Instance.RegisterController(app);
            LoadApplication(app);


        }

        protected override void OnResume()
        {
            while (MessageStackContext.Instance.Count > 0)
            {
                var message = MessageStackContext.Instance.Pop();
                HandleMessage(message);
            }
            base.OnResume();
        }

        private void HandleMessage(MessageStackContext.Message message)
        {
            switch (message.Type)
            {
                case MessageStackContext.MessageType.LocalNotification:
                    var beaconId = (long) message.Data[Constants.BeaconIdIntentDataKey];
                    var contentId = (long) message.Data[Constants.ContentIdIntentDataKey];
                    ShowContent(beaconId, contentId);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void ShowContent(long beaconId, long contentId)
        {
            NexenProximityManager.Get().GetBeaconZone(new Long(beaconId), new Long(contentId), this);
        }

        public void OnError(APIError p0)
        {
        }

        public void OnResponse(Object p0)
        {
            var zone = p0 as BaseNexenZone;
            NexenProximityManager.Get().PushNotificationTappedForZone(zone);

            if (zone?.Content is HtmlContent content)
            {
                NavigationPageContext.Instance.Controller.ShowUrl(content.PageUrl);
            }
        }
    }
}

