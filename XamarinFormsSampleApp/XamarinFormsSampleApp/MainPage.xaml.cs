﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamarinFormsSampleApp
{
	public partial class MainPage : ContentPage
	{
	    private readonly INavigationPageController _navigationPageController;
		public MainPage(INavigationPageController navigationPageController)
		{
		    _navigationPageController = navigationPageController;

            InitializeComponent();
		}

	    protected override void OnAppearing()
	    {
	        DependencyService.Get<ILocationPermissionHandler>().StartManagerOrShowRequestPermissions(_navigationPageController);
        }
	}
}
