﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinFormsSampleApp
{
    public interface INavigationPageController
    {
        void ShowUrl(string url);
    }
}
