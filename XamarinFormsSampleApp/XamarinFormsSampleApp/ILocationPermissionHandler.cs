﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinFormsSampleApp
{
    public interface ILocationPermissionHandler
    {
        void StartManagerOrShowRequestPermissions(INavigationPageController navigationPageController);
        void StopManager();
    }
}
