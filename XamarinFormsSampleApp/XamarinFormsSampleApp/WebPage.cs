﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace XamarinFormsSampleApp
{
    public class WebPage : ContentPage
    {
        private readonly WebView _browser;
        public WebPage(string url)
        {
            _browser = new WebView();

            _browser.Source = url;

            Content = _browser;
        }

    }
}
