﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Diagnostics;

using Xamarin.Forms;

namespace XamarinFormsSampleApp
{
	public partial class App : Application, INavigationPageController
    {
	    private readonly NavigationPage _navigationPage;
        public App ()
		{
			InitializeComponent();
            _navigationPage = new NavigationPage(new MainPage(this));
			MainPage = _navigationPage;
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}

	    public void ShowUrl(string url)
	    {
	        _navigationPage.PushAsync(new WebPage(url));
	    }
	}
}
