﻿using CoreLocation;
using NXBeaconSDK;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(XamarinFormsSampleApp.iOS.LocationPermissionHandler))]
namespace XamarinFormsSampleApp.iOS
{
    public class LocationPermissionHandler : ILocationPermissionHandler
    {

        public void StartManagerOrShowRequestPermissions(INavigationPageController navigationPageController)
        {

            void ShowContent(INXProximityZone zone)
            {
                ManagerContext.Instance.PushNotificationTappedForZone(zone);
                var content = zone.Content as NXHTMLProximityZoneContent;
                if (content != null)
                {
                    navigationPageController.ShowUrl(content.PageUrl);
                }
            }


            if (CLLocationManager.Status == CLAuthorizationStatus.AuthorizedAlways || CLLocationManager.Status == CLAuthorizationStatus.AuthorizedWhenInUse)
            {
                ManagerContext.Instance.StartManager(ShowContent);
            }
            else
            {
                var locMgr = new CLLocationManager
                {
                    PausesLocationUpdatesAutomatically = false
                };

                locMgr.AuthorizationChanged += (sender, e) =>
                {
                    if (e.Status == CLAuthorizationStatus.AuthorizedAlways || e.Status == CLAuthorizationStatus.AuthorizedWhenInUse)
                        ManagerContext.Instance.StartManager(ShowContent);
                };

                if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
                {
                    locMgr.RequestAlwaysAuthorization();
                }

                if (UIDevice.CurrentDevice.CheckSystemVersion(9, 0))
                {
                    locMgr.AllowsBackgroundLocationUpdates = true;
                }
            }
        }

        public void StopManager()
        {
            ManagerContext.Instance.StopManager();
        }
    }
}