﻿using System;
using Foundation;
using NXBeaconSDK;
using UIKit;

namespace XamarinFormsSampleApp.iOS
{
    public class ManagerContext
    {
        private static ManagerContext _instance;
        private readonly NXProximityManager _manager;

        public delegate void ShowContentDelegate(INXProximityZone zone);

        public static ManagerContext Instance => _instance ?? (_instance = new ManagerContext());


        private ManagerContext()
        {
            _manager = new NXProximityManager("<username>", "<password>", null, NSLocale.CurrentLocale);            
        }

        public void StartManager(ShowContentDelegate showContent)
        {
            StartManager(showContent, () => { });
        }

        public void StartManager(ShowContentDelegate showContent, Action onComplete)
        {
            void ContentTrigger(NSNotification notification)
            {
                if (UIApplication.SharedApplication.ApplicationState == UIApplicationState.Background)
                {
                    _manager.ShowNotification(notification);
                }
                else
                {
                    showContent(NXProximityManager.ProximityZoneFromNotification(notification));
                }
            }

            var center = NSNotificationCenter.DefaultCenter;
            center.AddObserver(new NSString("NXProximityManagerTriggerContentNotification"), ContentTrigger, _manager);

            _manager.Start(obj =>
            {
                onComplete();
            });
        }

        public void StopManager()
        {
            _manager.Reset();
            _instance = null;
        }

        public void PerformFetch(Action<UIBackgroundFetchResult> completionHandler)
        {
            _manager.PerformFetchWithCompletionHandler(completionHandler);
        }

        public void HandleEventsForBackgroundUrlSession(string sessionIdentifier, Action completionHandler)
        {
            _manager.HandleEventsForBackgroundURLSession(sessionIdentifier, completionHandler);
        }

        public INXProximityZone GetProximityZone(string beaconId, int contentType)
        {
            return _manager.ProximityZoneForIdentifier(beaconId, contentType);
        }

        public void PushNotificationTappedForZone(INXProximityZone zone)
        {
            _manager.IncrementLoyaltyForProximityZone(zone);
        }

    }
}