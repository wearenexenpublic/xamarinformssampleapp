﻿using System;
using CoreLocation;
using Foundation;
using NXBeaconSDK;
using UIKit;
using UserNotifications;

namespace XamarinFormsSampleApp.iOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the 
	// User Interface of the application, as well as listening (and optionally responding) to 
	// application events from iOS.
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
	    public delegate void ShowContentForNotificationDelegate(string beaconId, int contentType);

        private class CustomUserNotificationCenterDelegate : UNUserNotificationCenterDelegate
        {
            private readonly ShowContentForNotificationDelegate _showContentForNotification;

            public CustomUserNotificationCenterDelegate(ShowContentForNotificationDelegate showContentForNotification)
            {
                _showContentForNotification = showContentForNotification;
            }

            public override void DidReceiveNotificationResponse(UNUserNotificationCenter center, UNNotificationResponse response, Action completionHandler)
            {
                var beaconId = response.Notification.Request.Content.UserInfo["beaconIdentifier"].ToString();
                var contentType = int.Parse(response.Notification.Request.Content.UserInfo["contentType"].ToString());

                _showContentForNotification(beaconId, contentType);
                // Handle notification tap here
                completionHandler();
            }

        }


        private ManagerContext _manager = ManagerContext.Instance;
	    private XamarinFormsSampleApp.App _app;

        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
		    _manager = ManagerContext.Instance;
            global::Xamarin.Forms.Forms.Init ();
		    _app = new XamarinFormsSampleApp.App();
            LoadApplication (_app);

		    if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
		    {
		        var notificationSettings = UIUserNotificationSettings.GetSettingsForTypes(
		            UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, null
		        );

                app.RegisterUserNotificationSettings(notificationSettings);
		    }

		    UNUserNotificationCenter.Current.Delegate = new CustomUserNotificationCenterDelegate(ShowContent); 
            return base.FinishedLaunching (app, options);
		}

	    public override void WillEnterForeground(UIApplication application)
	    {
	        if (CLLocationManager.Status == CLAuthorizationStatus.AuthorizedAlways)
	        {
	            _manager.StartManager(ShowContent);
	        }
	    }

	    public override void HandleEventsForBackgroundUrl(UIApplication application, string sessionIdentifier, Action completionHandler)
        { 
	        _manager?.HandleEventsForBackgroundUrlSession(sessionIdentifier, completionHandler);
	    }

	    public override void PerformFetch(UIApplication application, Action<UIBackgroundFetchResult> completionHandler)
	    {
	        _manager?.PerformFetch(completionHandler);
	    }

        public override void ReceivedLocalNotification(UIApplication application, UILocalNotification notification)
        {
            var beaconId = notification.UserInfo["beaconIdentifier"].ToString();
            var contentType = int.Parse(notification.UserInfo["contentType"].ToString());

            ShowContent(beaconId, contentType);

        }

        private void ShowContent(string beaconId, int contentType)
	    {
	        ShowContent(_manager.GetProximityZone(beaconId, contentType));          
	    }

	    private void ShowContent(INXProximityZone zone)
	    {
	        _manager.PushNotificationTappedForZone(zone);
	        var content = zone.Content as NXHTMLProximityZoneContent;
	        if (content != null)
	        {
	            _app.ShowUrl(content.PageUrl);
            }

        }
    }
}
